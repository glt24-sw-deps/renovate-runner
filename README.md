# renovate-runner

This repository contains a minimal CI configuration to run [**renovate**](https://github.com/renovatebot/renovate). 

The CI file is based on the official [renovate-runner](https://gitlab.com/renovate-bot/renovate-runner/).

All configuration is done via env variables. The configuration restricts renovate to run only on projects in this `glt24-sw-deps` group, and only if a configuration file is present in the repository (e.g. `renovate.json`).